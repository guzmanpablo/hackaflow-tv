import { createContext, useContext, useState, useEffect } from "react";
import useWebSocket from '../hooks/useWebSocket'

const Context = createContext();
const STORAGE = "session";

export const SessionProvider = ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem(STORAGE) || '');
  const { code, status } = useWebSocket()

  const signInCode = (token) => {
    localStorage.setItem(STORAGE, token);
    setToken(token);
  };

  const signOut = () => {
    localStorage.removeItem(STORAGE);
    setToken('');
  };

  useEffect(() => {
    if (status === 'device_provisioned') signInCode('shalala')
    else signInCode('')
  }, [status])

  const value = {
    code,
    signInCode,
    signOut,
    logged: !!token,
  };

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export const useSession = () => useContext(Context);
