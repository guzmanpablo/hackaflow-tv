import Button from "../atoms/Button";
import Canvas from "../atoms/Canvas";
import Card from "../atoms/Card";
import Code from "../atoms/Code";
import Container from "../atoms/Container";
import Header from "../atoms/Header";
import { useSession } from '../modules/Session'

const Login = () => {
  const { code } = useSession();
  
  return (
    <Container className="justify-between">
      <Header title="Inicio rápido de sesión" className="mb-14 sm:mb-12"/>
      <article className="w-full flex justify-between items-start my-12">
        <Card
          className="w-1/2"
          title="Inicio web"
          subtitle="Ingresá en la página flow.com.ar/activar e ingresá el siguiente
          código:"
        >
          <div className="flex justify-center">
            {code && <Code code={code} />}
          </div>
        </Card>
        <Card
          className="w-1/2"
          title="Inicio movil"
          subtitle="Usa el lector de códigos QR de tu smartphone, tablet u otro
        dispositivo móvil."
        >
          <div className="flex justify-center">
            {code && <Canvas code={code} />}
          </div>
        </Card>
      </article>
      <footer className="text-lg text-white text-center text-gray-400 my-12">
        <p className="text-lg text-white text-center text-gray-400">
          Una vez que la pantalla reconozca el código o el QR, la plataforma se
          actualizará automáticamente.
        </p>
        <Button className="my-6" >Ingreso Manual</Button>
      </footer>
    </Container>
  );
};

export default Login;
