import Button from "../atoms/Button";
import Container from "../atoms/Container";
import Header from "../atoms/Header";
import { useSession } from "../modules/Session";

const Home = () => {
  const session = useSession();

  return (
    <Container>
      <Header title="Bienvenido" />
      <article className="w-full h-5/6 items-start py-16">
        <h2 className="text-white text-2xl mb-4">La magia de flow</h2>
        <Button onClick={session.signOut}>Cerrar sesión</Button>
      </article>
    </Container>
  );
};

export default Home;
