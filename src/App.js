import { useSession } from "./modules/Session"
import Home from "./pages/Home";
import Login from "./pages/Login"


function App() {
  const { logged } = useSession()

  return logged ? <Home /> : <Login />;

}

export default App;
