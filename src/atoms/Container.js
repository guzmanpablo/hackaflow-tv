import PropTypes from "prop-types";

const Container = ({ children, className = '' }) => (
  <div
    className={
      "w-full min-h-screen bg-gradient-to-b from-black to-gray-900 justify-center flex"
    }
  >
    <main className={`w-11/12 flex flex-col ${className}`}>{children}</main>
  </div>
);

Container.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default Container;
