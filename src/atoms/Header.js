import PropTypes from "prop-types";

const Header = ({ title, className = ''}) => (
  <header
    className={`w-full flex justify-between items-center mt-16 ${className}`}
  >
    <h1 className="text-5xl sm:4-xl text-white pt-2">{title}</h1>
    <img className="w-1/12 sm:w-32" src="../assets/logo.png" alt="Flow" />
  </header>
);

Header.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
};

export default Header;
