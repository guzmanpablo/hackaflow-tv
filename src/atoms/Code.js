import PropTypes from "prop-types";

const Code = ({ code, className = '' }) => (
  <div className={`flex ${className}`}>
    {[...code].map((key) => (
      <p key={key} className="text-6xl text-white underline mx-2">
        {key}
      </p>
    ))}
  </div>
);

Code.propTypes = {
  className: PropTypes.string,
  code: PropTypes.string.isRequired,
};

export default Code;
