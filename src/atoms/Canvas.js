import { useEffect, useRef } from "react";
import { generate } from "lean-qr";
import PropTypes from "prop-types";
import "./Canvas.css";

const Canvas = ({ code, className = '' }) => {
  const canvas = useRef();

  useEffect(() => {
    if (!canvas) return;
    const qr = generate(code);
    qr.toCanvas(canvas.current);
  }, [code, canvas]);

  return (
    <div className={`bg-white ${className}`}>
      <canvas ref={canvas} className="qr-canvas" />
    </div>
  );
};

Canvas.propTypes = {
  className: PropTypes.string,
  code: PropTypes.string.isRequired,
};

export default Canvas;
