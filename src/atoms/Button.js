import React from "react";
import PropTypes from "prop-types";

const Button = ({ children, className = '', onClick }) => (
  <button
    onClick={onClick}
    className={`bg-green-600 outline-none focus:outline-none hover:bg-green-700 rounded px-4 py-2 text-xl text-white ${className}`}
  >
    {children}
  </button>
);

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default Button;
