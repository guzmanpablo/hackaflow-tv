import React from "react";
import PropTypes from "prop-types";

const Card = ({ title, subtitle, className = "", children }) => (
  <section className={`h-auto w-auto ${className}`}>
    <h2 className="text-center text-gray-200 text-4xl text-white mb-4">
      {title}
    </h2>
    <p className="text-center text-gray-400 text-lg sm:text-md text-white mb-6">
      {subtitle}
    </p>
    {children}
  </section>
);

Card.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default Card;
