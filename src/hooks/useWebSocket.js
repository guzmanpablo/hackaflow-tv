import { useEffect, useState, useCallback } from "react"
import Cookies from 'universal-cookie'
import { io } from "socket.io-client"
const COOKIE_NAME = 'hackaflow'
const useWebSocket = () => {
    const [status, setStatus] = useState('');
    const [socket, setSocket] = useState(null)
    const [code, setCode] = useState(null);
    const cookies = new Cookies()
    
    const createClientID = () => Math.random().toString(36).substring(2, 15).toUpperCase()

    const getCookieCode = () => cookies.get(COOKIE_NAME)

    const createCookie = (clientId) => {
        const option = { maxAge: 60 * 60 }
        cookies.set(COOKIE_NAME, clientId, option)
    }

    const handleEasyLogin = (id) => (data) => {
        if (!getCookieCode()) createCookie(id);
        const { code } = data
        setCode(code)
    };

    const handleEasyLoginSuccess = ({ status, code }) => {
        !!code && setCode(code);
        !!status && setStatus(status);
    };

    const easyLogin = () => {
        const client_id = getCookieCode() || createClientID()
        socket.emit('easy-login', { client_id }, handleEasyLogin(client_id))
        socket.on(client_id, handleEasyLoginSuccess)
    };

    const connect = () => {
        const socket = io("https://hackaflow-team3-backend.herokuapp.com")
        if (socket) setSocket(socket)
    }

    useEffect(() => {
        socket && easyLogin()
    }, [socket])

    useEffect(() => {
        connect();
    }, [])
    
    return { code, status }
}
export default useWebSocket