import { useState } from "react"

const useApi = () => {
    const [apiResponse, setApiResponse] = useState(null)
    const [deviceID, setDeviceID] = useState(null)

    const createDeviceID = () => {
        const deviceID = 'identificador_dispositivo'
        setDeviceID(deviceID)
        return deviceID
    }

    const getCode = () => {
        const id = createDeviceID()
        const url = 'https//'

        const postID = async (url, id) => {
            const response = await fetch (url, {
                method: 'POST',
                body: JSON.stringify(id)
            })
            const data = response.json()
            return data
        }

        // const data = await postID(url, id)
        // setApiResponse(data)
    }

    return { apiResponse, deviceID, getCode }
}

export default useApi;
